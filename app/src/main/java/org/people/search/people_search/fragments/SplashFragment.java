package org.people.search.people_search.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.people.search.people_search.R;
import org.people.search.people_search.people_search.Const;

public class SplashFragment extends AdFragment {

    public SplashFragment() {}

    private static final int SPLASH_DURATION = 7000;
    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState ) {
        View rootView = inflater.inflate( R.layout.fragment_splash, container, false );
        //rootView.findViewById( R.id.start ).setOnClickListener( this );

        initInterstitialAd();
        splashDuration();
        return rootView;
    }

    private void splashDuration(){
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startApp();
            }
        },SPLASH_DURATION );
    }

    private void startApp(){
        if ( checkIfInternetConnectionActive() ) {
            showInterstitialAd();
        } else {
            showInternetConnectionAlert();
        }
    }

    @Override
    int getCurrentFrame() {
        return Const.SPLASH_FRAGMENT;
    }


    private void showInternetConnectionAlert() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder( getContext() );
        builder.setTitle( getString( R.string.app_name ) )
                .setMessage( getString( R.string.internet_connection_error_message ) )
                .setCancelable( true )
                .setNegativeButton( R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick( DialogInterface dialog, int id ) {
                                dialog.cancel();
                                splashDuration();
                            }
                        } );

        android.support.v7.app.AlertDialog alert = builder.create();
        alert.show();
    }

    private boolean checkIfInternetConnectionActive() {
        ConnectivityManager cm = ( ConnectivityManager ) getContext().getSystemService( Context.CONNECTIVITY_SERVICE );
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }
}