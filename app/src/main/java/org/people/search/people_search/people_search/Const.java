package org.people.search.people_search.people_search;

public class Const {

    public static final String NEXT_PAGE             = "org.people.search.prank_application.NEXT_PAGE";
    public static final String SHOW_WEB_VIEW         = "org.people.search.prank_application.SHOW_WEB_VIEW";
    public static final String EXIT                  = "org.people.search.prank_application.EXIT";
    public static final String UPDATE_SEARCH_RESULTS = "org.people.search.prank_application.UPDATE_SEARCH_RESULTS";
    public static final String UPDATE_MAP_POSITION   = "org.people.search.prank_application.UPDATE_MAP_POSITION";

    public static final String PREFERENCES           = "org.people.search.prank_application.PREFERENCES";
    public static final String PREFERENCES_RATING    = "org.people.search.prank_application.PREFERENCES_RATING";
    public static final String PREFERENCES_VERSION   = "org.people.search.prank_application.VERSION";


    public static final int SPLASH_FRAGMENT                 = 0;
    public static final int SUGGESTED_APPLICATIONS_FRAGMENT = 1;
    public static final int RATING_FRAGMENT                 = 2;
    public static final int ENTER_NAME_FRAGMENT             = 3;
    public static final int SEARCH_RESULT_FRAGMENT          = 4;
    public static final int MAP_DETAILS_FRAGMENT            = 5;
    public static final int DETAILED_REPORT_FRAGMENT        = 6;
    public static final int THANKS_FRAGMENT                 = 7;
    public static final int WEB_VIEW_CONTAINER_FRAGMENT     = 8;

    public static final int SEND_SUCCESSFULLY = 0;
    public static final int SEND_WITH_ERROR   = 1;
}
