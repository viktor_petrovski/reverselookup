package org.people.search.people_search;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

public class App extends Application {

    // Create the instance
    private static App instance;
    public static App getInstance()
    {
        if (instance== null) {
            synchronized(instance) {
                if (instance == null)
                    instance = new App();
            }
        }
        // Return the instance
        return instance;
    }

    public App()
    {
        // Constructor hidden because this is a singleton
    }

    @Override
    public void onCreate() {
        super.onCreate();

        FacebookSdk.sdkInitialize( getApplicationContext() );

        GoogleAnalytics analytics = GoogleAnalytics.getInstance( this );
        GoogleAnalytics.getInstance( this ).setLocalDispatchPeriod( 15 );

        Tracker tracker = analytics.newTracker( getString( R.string.google_analytics ) );
        tracker.enableExceptionReporting( true );
    }
}
