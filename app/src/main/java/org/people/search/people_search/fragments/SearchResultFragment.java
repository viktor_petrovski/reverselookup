package org.people.search.people_search.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;

import org.people.search.people_search.R;
import org.people.search.people_search.people_search.AddressesKeeper;
import org.people.search.people_search.people_search.Const;
import org.people.search.people_search.people_search.SearchKeeper;
import org.people.search.people_search.fragments.adapter.SearchResultsAdapter;
import org.people.search.people_search.http.json.SearchResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class SearchResultFragment extends AdFragment implements View.OnClickListener {

    private ListView listView;
    private SearchResultsAdapter adapter;

    private ProgressDialog dialog;

    private final String PREF = "org.people.search.people_search";
    private final List<String> emails = Arrays.asList(new String[]
                                                              {"@aol.com", "@att.net", "@comcast.net", "@facebook.com",
                                                                      "@gmail.com", "@gmx.com", "@googlemail.com",
                                                                      "@google.com", "@hotmail.com", "@hotmail.co.uk",
                                                                      "@mac.com", "@me.com", "@mail.com", "@msn.com",
                                                                      "@live.com", "@sbcglobal.net", "@verizon.net",
                                                                      "@yahoo.com", "@yahoo.co.uk"});

    private Handler handler = new Handler();
    private Random random = new Random(System.currentTimeMillis());

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            createProgressDialog();
            if (SearchKeeper.getPosition() == 0) {
                if (adapter != null) {
                    adapter.clear();
                    adapter.notifyDataSetChanged();
                    updateTotalCount();
                }

                dialog.show();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            dialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        adapter = new SearchResultsAdapter(getContext(), getSearchResults(getContext()));
                        listView.setAdapter(adapter);
                        updateTotalCount();
                    }
                }, random.nextInt(5) + 5 * 1000);
            } else {
                adapter.addAll(getSearchResults(getContext()));
                adapter.notifyDataSetChanged();
                updateTotalCount();
            }
        }
    };

    private void updateTotalCount() {
        TextView textView = (TextView) getView().findViewById(R.id.total_count);
        if (SearchKeeper.getSize() == 0) {
            textView.setText("");
        } else {
            textView.setText(getString(R.string.total_found).replace("{N}", getTotalCount()));
        }
    }

    private String getTotalCount() {
        if (SearchKeeper.getSearch_cirteria() == SearchKeeper.SEARCH_BY_NAME)
            return String.valueOf(2000 - SearchKeeper.getState().length() * 23
                                          - SearchKeeper.getName().length() * 15);
        if (SearchKeeper.getSearch_cirteria() == SearchKeeper.SEARCH_BY_EMAIL)
            return String.valueOf(2000 + SearchKeeper.getEmail().length() * 4);
        if (SearchKeeper.getSearch_cirteria() == SearchKeeper.SEARCH_BY_PHONE)
            return String.valueOf(2000 - SearchKeeper.getState().length() * 23
                                          - SearchKeeper.getPhone().length() * 15);
        return String.valueOf(adapter.getCount());
    }

    private void createProgressDialog() {
        if (dialog == null) {
            dialog = new ProgressDialog(getContext());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setMessage(getString(R.string.loading));
            dialog.setCancelable(false);
        }
    }

    public SearchResultFragment() {
    }

    @Override
    int getCurrentFrame() {
        return Const.SEARCH_RESULT_FRAGMENT;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(receiver, new IntentFilter(Const.UPDATE_SEARCH_RESULTS));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(receiver);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_search_result, container, false);
        initAdBanner(rootView);
        initInterstitialAd();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        initWidgets(getView());
    }

    private void initWidgets(View rootView) {
        listView = (ListView) rootView.findViewById(R.id.search_results);
        if (listView.getFooterViewsCount() == 0) {
            View footerView = ((LayoutInflater) getContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer_layout, null, false);
            footerView.findViewById(R.id.search_results_more).setOnClickListener(this);
            listView.addFooterView(footerView);
        }
    }

    private List<SearchResult> getSearchResults(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREF, Activity.MODE_PRIVATE);
        Set<String> savedResults = preferences.getStringSet(getCurrentSearchKey(), new HashSet());
        if (savedResults.size() == 0) {
            List<SearchResult> allResults;
            if (SearchKeeper.getSearch_cirteria() == SearchKeeper.SEARCH_BY_NAME)
                allResults = AddressesKeeper.getResultsByNameAndState(SearchKeeper.getState(), SearchKeeper.getName());
            else if (SearchKeeper.getSearch_cirteria() == SearchKeeper.SEARCH_BY_PHONE)
                allResults = AddressesKeeper.getResultsByPhoneAndState(SearchKeeper.getState(), SearchKeeper.getPhone());
            else
                allResults = AddressesKeeper.getResultsByEmail(SearchKeeper.getEmail());

            List<SearchResult> results = getRandomSubList(allResults, 12);
            //updateResults( results );
            saveResults(results, preferences);
            SearchKeeper.setSize(results.size());

            return getResults(results);
        } else {
            List<SearchResult> results = getSavedResults(savedResults);
            SearchKeeper.setSize(results.size());
            Collections.sort(results, new Comparator<SearchResult>() {
                @Override
                public int compare(SearchResult lhs, SearchResult rhs) {
                    return lhs.getId() - rhs.getId();
                }
            });

            return getResults(results);
        }
    }

    private List<SearchResult> getResults(List<SearchResult> results) {
        int currentPosition = SearchKeeper.getPosition();
        SearchKeeper.setPosition(SearchKeeper.getPosition() + 2);

        if (currentPosition < results.size() && currentPosition + 2 <= results.size()) {
            return results.subList(currentPosition, currentPosition + 2);
        } else if (currentPosition < results.size() && currentPosition + 2 > results.size()) {
            return results.subList(currentPosition, results.size());
        } else {
            return new LinkedList();
        }
    }

    private List<SearchResult> getSavedResults(Set<String> savedResults) {
        List<SearchResult> allResults = AddressesKeeper.getResultsByState(SearchKeeper.getState());
        List<SearchResult> result = new LinkedList();
        for (String saved : savedResults) {
            String[] parts = saved.split("__");

            int id = Integer.parseInt(parts[0]);
            //String email = parts[1];
            for (SearchResult searchResult : allResults) {
                if (searchResult.getId() == id) {
                    //searchResult.setEmail(email);
                    result.add(searchResult);
                }
            }
        }

        return result;
    }

    private void saveResults(List<SearchResult> results, SharedPreferences preferences) {
        SharedPreferences.Editor editor = preferences.edit();
        Set<String> values = new HashSet();
        for (SearchResult result : results) {
            values.add(String.valueOf(result.getId()) + "__" + result.getEmail());
        }

        editor.putStringSet(getCurrentSearchKey(), values);
        editor.apply();
    }

    private void updateResults(List<SearchResult> results) {
        for (SearchResult result : results) {
            result.setEmail(generateEmail(random.nextInt(5),
                    random.nextInt(emails.size()),
                    random.nextInt(15),
                    random.nextInt(2),
                    random.nextInt(15),
                    random.nextInt(2)));
        }
    }

    private String generateEmail(int mode, int email, int prefix, int needYear, int suffix, int nameCrop) {
        String name = getName(mode, nameCrop);
        String strPrefix = getPrefix(prefix);
        String strSuffix = getSuffix(suffix);

        return strPrefix + name + strSuffix +
                       (needYear > 0 ? String.valueOf(1970 + random.nextInt(30)) : "")
                       + emails.get(email);
    }

    private String getSuffix(int suffix) {
        String strSuffix;

        if (suffix == 0) {
            strSuffix = "work";
        } else if (suffix == 1) {
            strSuffix = "boss";
        } else if (suffix == 2 || suffix == 3) {
            strSuffix = "god";
        } else {
            strSuffix = "";
        }

        return strSuffix;
    }

    private String getPrefix(int prefix) {
        String strPrefix;

        if (prefix == 0) {
            strPrefix = "mr";
        } else if (prefix == 1) {
            strPrefix = "mc";
        } else {
            strPrefix = "";
        }

        return strPrefix;
    }

    private String getName(int mode, int nameCrop) {
        String name;

        if (mode == 0) {
            name = SearchKeeper.getName().trim().replaceAll(" ", "_");
            if (nameCrop > 0) {
                int index = name.lastIndexOf("_");
                if (index > 0) {
                    name = name.substring(0, index);
                }
            }
        } else if (mode == 1) {
            name = SearchKeeper.getName().trim().replace(" ", ".");
            if (nameCrop > 0) {
                int index = name.lastIndexOf(".");
                if (index > 0) {
                    name = name.substring(0, index);
                }
            }
        } else if (mode == 2) {
            name = SearchKeeper.getName();

            if (nameCrop > 0) {
                int index = name.lastIndexOf(" ");
                if (index > 0) {
                    name = name.substring(0, index);
                }
            }

            name = name.trim().replaceAll(" ", "");
        } else if (mode == 3) {
            String[] parts = SearchKeeper.getName().split(" ");
            name = "";
            for (String part : parts) {
                name = part + " " + name;
            }

            name = name.trim().replaceAll(" ", "_");

            if (nameCrop > 0) {
                int index = name.lastIndexOf("_");
                if (index > 0) {
                    name = name.substring(0, index);
                }
            }
        } else {
            String[] parts = SearchKeeper.getName().split(" ");
            name = "";
            for (String part : parts) {
                name = part + " " + name;
            }
            name = name.trim().replaceAll(" ", ".");

            if (nameCrop > 0) {
                int index = name.lastIndexOf(".");
                if (index > 0) {
                    name = name.substring(0, index);
                }
            }
        }

        return name;
    }

    private static List<SearchResult> getRandomSubList(List<SearchResult> list, int size) {

        if (list.size() < size)
            size = list.size();

        list = new ArrayList(list);
        Collections.shuffle(list);

        List<SearchResult> result = list.subList(0, size);
        Collections.sort(result, new Comparator<SearchResult>() {
            @Override
            public int compare(SearchResult lhs, SearchResult rhs) {
                return lhs.getId() - rhs.getId();
            }
        });
        return result;
    }

    private String getCurrentSearchKey() {
        if (SearchKeeper.getSearch_cirteria() == SearchKeeper.SEARCH_BY_NAME) {
            return SearchKeeper.getState().replaceAll(" ", "") + "__" +
                           SearchKeeper.getName().replaceAll(" ", "_").replace("'", "");
        } else if (SearchKeeper.getSearch_cirteria() == SearchKeeper.SEARCH_BY_PHONE) {
            return SearchKeeper.getState().replaceAll(" ", "") + "__" +
                           SearchKeeper.getPhone().replaceAll(" ", "_").replace("'", "");
        } else if (SearchKeeper.getSearch_cirteria() == SearchKeeper.SEARCH_BY_EMAIL)
            return SearchKeeper.getEmail();
        return "";
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.search_results_more:
                showInterstitialAd();
                break;
        }
    }

    @Override
    protected boolean customOnAdClosedHandler() {
        loadMoreData();
        return true;
    }

    private void loadMoreData() {
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(new Intent(Const.UPDATE_SEARCH_RESULTS));
    }
}
