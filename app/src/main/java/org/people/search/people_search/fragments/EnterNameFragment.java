package org.people.search.people_search.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.people.search.people_search.R;
import org.people.search.people_search.Util;
import org.people.search.people_search.people_search.AddressesKeeper;
import org.people.search.people_search.people_search.Const;
import org.people.search.people_search.people_search.SearchKeeper;

import java.util.List;

public class EnterNameFragment extends AdFragment implements View.OnClickListener {

    EditText nameEditText;
    AutoCompleteTextView stateEditText;

    EditText etPhone;
    EditText etEmail;

    LinearLayout llStateNameHolder;
    LinearLayout llEmailHolder;

    TextInputLayout inputEmail;
    TextInputLayout inputPhone;
    TextInputLayout inputName;
    TextInputLayout inputState;

    String version;

    public EnterNameFragment() {
    }

    @Override
    int getCurrentFrame() {
        return Const.ENTER_NAME_FRAGMENT;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_enter_name, container, false);
        initAdBanner(rootView);

        nameEditText = (EditText) rootView.findViewById(R.id.name);
        stateEditText = (AutoCompleteTextView) rootView.findViewById(R.id.state);

        etPhone = (EditText) rootView.findViewById(R.id.phone);

        etEmail = (EditText) rootView.findViewById(R.id.email);

        llStateNameHolder = (LinearLayout) rootView.findViewById(R.id.ll_state_name_holder);
        llEmailHolder = (LinearLayout) rootView.findViewById(R.id.ll_email_holder);

        inputEmail = (TextInputLayout) rootView.findViewById(R.id.text_input_email);
        inputPhone = (TextInputLayout) rootView.findViewById(R.id.text_input_phone);
        inputName = (TextInputLayout) rootView.findViewById(R.id.text_input_name);
        inputState = (TextInputLayout) rootView.findViewById(R.id.text_input_state);

        return rootView;
    }

    private void setupViewsCorrectly(){
        version = Util.getSearchVersion(getContext());
        switch (version) {
            case "phone":
                //name
                nameEditText.setVisibility(View.GONE);
                stateEditText.setVisibility(View.VISIBLE);
                //email
                llEmailHolder.setVisibility(View.GONE);
                //phone
                etPhone.setVisibility(View.VISIBLE);
                break;
            case "name":
                //name
                nameEditText.setVisibility(View.VISIBLE);
                stateEditText.setVisibility(View.VISIBLE);
                llStateNameHolder.setVisibility(View.VISIBLE);
                //email
                llEmailHolder.setVisibility(View.GONE);
                //phone
                etPhone.setVisibility(View.GONE);
                break;
            case "email":
                llEmailHolder.setVisibility(View.VISIBLE);
                llStateNameHolder.setVisibility(View.GONE);
                etPhone.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onResume() {
        super.onResume();
        initWidgets(getView());
        setupViewsCorrectly();
    }

    private void initWidgets(View rootView) {
        rootView.findViewById(R.id.next).setOnClickListener(this);

        AutoCompleteTextView stateEditText = (AutoCompleteTextView) getView().findViewById(R.id.state);
        stateEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    tryToOpenNextScreen();
                    return true;
                }

                return false;
            }
        });
        stateEditText.setAdapter(new ArrayAdapter(getContext(),
                                                         android.R.layout.simple_dropdown_item_1line, AddressesKeeper.getStates()));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.next:
                tryToOpenNextScreen();
                break;
        }
    }

    private void tryToOpenNextScreen() {

        inputEmail.setError(null);
        inputName.setError(null);
        inputPhone.setError(null);
        inputState.setError(null);

        List<String> states = AddressesKeeper.getStates();
        switch (version) {
            case "name":
                if (states.contains(stateEditText.getText().toString())) {
                    if (nameEditText.getText().length() > 0 && stateEditText.length() > 0) {
                        updateSearchKeeper(stateEditText.getText().toString(), nameEditText.getText().toString());
                        showInterstitialAd();
                    } else {
                        inputName.setError(getString(R.string.field_cant_be_empty));
                    }
                } else {
                    showWrongStateErrorMessage();
                }
                break;
            case "phone":
                String phoneString = etPhone.getText().toString().trim();
                if (states.contains(stateEditText.getText().toString())) {
                    if (phoneString.length() == 10 && stateEditText.length() > 0) {
                        String finalPhoneString = "(" + phoneString.substring(0, 3) + ")" + " " + phoneString.substring(3, 6) + "-" + phoneString.substring(6);
                        updateSearchKeeperWithPhone(stateEditText.getText().toString(), finalPhoneString);
                        showInterstitialAd();
                    } else {
                        inputPhone.setError(getString(R.string.invalid_phone_number));
                        //showFillFieldErrorMessage();
                    }
                } else {
                    showWrongStateErrorMessage();
                }
                break;
            case "email":
                String emailString = etEmail.getText().toString().trim();
                if (emailString.length() > 0) {
                    updateSearchKeeperWithEmail(emailString);
                    showInterstitialAd();
                } else
                    inputEmail.setError(getString(R.string.invalid_email_address));
                break;
        }
    }

    private void updateSearchKeeper(String state, String name) {
        SearchKeeper.resetData();
        SearchKeeper.setName(name);
        SearchKeeper.setState(state);
        SearchKeeper.setSearch_cirteria(SearchKeeper.SEARCH_BY_NAME);
    }

    private void updateSearchKeeperWithPhone(String state, String phone) {
        SearchKeeper.resetData();
        SearchKeeper.setState(state);
        SearchKeeper.setPhone(phone);
        SearchKeeper.setSearch_cirteria(SearchKeeper.SEARCH_BY_PHONE);
    }

    private void updateSearchKeeperWithEmail(String email) {
        SearchKeeper.resetData();
        SearchKeeper.setEmail(email);
        SearchKeeper.setSearch_cirteria(SearchKeeper.SEARCH_BY_EMAIL);
    }

    private void showWrongStateErrorMessage() {
        inputState.setError(getString(R.string.invalid_state));
        return;

//        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//        builder.setMessage(R.string.dialog_wrong_state_message)
//                .setNegativeButton(R.string.dialog_close_message_cancel,
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                dialog.dismiss();
//                            }
//                        });
//
//        builder.create().show();
    }

    private void showFillFieldErrorMessage() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(R.string.dialog_fill_field_message)
                .setNegativeButton(R.string.dialog_close_message_cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });

        builder.create().show();
    }
}
