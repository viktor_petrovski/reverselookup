package org.people.search.people_search.http;

import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.ResponseBody;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import org.people.search.people_search.people_search.Const;
import org.people.search.people_search.http.json.Subscriber;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import retrofit.JacksonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class SendEmailTask extends AsyncTask< String, Integer, Integer > {

    @Override
    protected Integer doInBackground( final String... params ) {
        final OkHttpClient okHttpClient = new OkHttpClient();

        okHttpClient.setReadTimeout( 60, TimeUnit.SECONDS );
        okHttpClient.setConnectTimeout( 10, TimeUnit.SECONDS );
        okHttpClient.setWriteTimeout( 60, TimeUnit.SECONDS );
        okHttpClient.interceptors().add( new Interceptor() {
            @Override
            public com.squareup.okhttp.Response intercept( Chain chain ) throws IOException {
                Request original = chain.request();

                String credentials = params[ 3 ] + ":x";
                String basic = "Basic " + Base64.encodeToString( credentials.getBytes(), Base64.NO_WRAP );

                Request.Builder requestBuilder = original.newBuilder()
                        .header( "Authorization", basic )
                        .header( "Accept", "application/json" )
                        .method( original.method(), original.body() );

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        } );

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel( HttpLoggingInterceptor.Level.BODY );
        okHttpClient.interceptors().add(interceptor);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl( params[ 0 ] )
                .addConverterFactory( JacksonConverterFactory.create() )
                .client( okHttpClient )
                .build();

        ServerService service = retrofit.create( ServerService.class );

        try {
            Response< ResponseBody > response = service.addSubscriber(
                    new Subscriber( params[ 1 ] ),
                    params[ 2 ] ).execute();

            if ( response.code() == 200 || response.code() == 201 ) {
                return Const.SEND_SUCCESSFULLY;
            }
        } catch ( Exception e ) {
            Log.e( SendEmailTask.class.getName(), "", e );
            e.printStackTrace();
        }

        return Const.SEND_WITH_ERROR;
    }
}
