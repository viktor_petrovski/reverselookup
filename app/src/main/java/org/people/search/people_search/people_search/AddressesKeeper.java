package org.people.search.people_search.people_search;

import android.content.Context;
import android.os.AsyncTask;

import org.people.search.people_search.R;
import org.people.search.people_search.http.json.SearchResult;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class AddressesKeeper {

    private static boolean inited = false;
    private static Map<String, List<SearchResult>> stateResults = new HashMap();
    private static Map<String, List<SearchResult>> phoneResults = new HashMap();
    private static Map<String, List<SearchResult>> emailResults = new HashMap();

    public static void downloadAddressesFromNet(final Context context) {
        AsyncTask<Void, Integer, Boolean> task = new AsyncTask<Void, Integer, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... params) {
                try {
                    URL url = new URL(context.getString(R.string.addresses_file_link));
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setConnectTimeout(60000);

                    BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    parseFile(reader);
                    return true;

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            }

            @Override
            protected void onPostExecute(Boolean isInited) {
                super.onPostExecute(isInited);
                if(!isInited)
                    getDataFromFile(context);
            }

        };
        task.execute();
    }

    public static void getDataFromFile(final Context context) {
        if (inited)
            return;
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                BufferedReader reader = null;
                try {
                    reader = new BufferedReader(new InputStreamReader(context.getAssets().open("addresses")));
                    parseFile(reader);
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
    }

    public static void parseFile(BufferedReader reader) {
        String line = null;
        try {
            line = reader.readLine();

            String state = null;
            int lineNumber = 0;

            while (line != null) {
                lineNumber++;

                if (line.startsWith("-")) {
                    state = line.split("-")[1];
                    stateResults.put(state, new LinkedList());

                } else {
                    String[] addressesArray = line.split(",");

                    String address = (addressesArray[0] + ", " + addressesArray[1] + ", " + addressesArray[2] + ", " + addressesArray[3] + ", " + addressesArray[4]).trim();
                    String phone = addressesArray[5];
                    String mail = addressesArray[6];
                    String name = addressesArray[7];

                    SearchResult result = new SearchResult(lineNumber, address, phone, name, mail);
                    stateResults.get(state).add(result);

                    if (phoneResults.get(phone) == null)
                        phoneResults.put(phone, new LinkedList<SearchResult>());
                    phoneResults.get(phone).add(result);

                    if (emailResults.get(mail) == null)
                        emailResults.put(mail, new LinkedList<SearchResult>());
                    emailResults.get(mail).add(result);

                }

                line = reader.readLine();
            }
            inited = true;
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void init(final Context context) {
        if (inited)
            return;
        downloadAddressesFromNet(context);
    }

    public static List<String> getStates() {
        List<String> result = new LinkedList();
        result.addAll(stateResults.keySet());
        return result;
    }

    public static List<String> getPhones() {
        List<String> result = new LinkedList();
        result.addAll(phoneResults.keySet());
        return result;
    }

    public static List<SearchResult> getResultsByState(String state) {
        return stateResults.get(state);
    }

    public static List<SearchResult> getResultsByNameAndState(String state, String name) {
        List<SearchResult> results = stateResults.get(state);
        List<SearchResult> resultsList = new LinkedList<>();
        for (SearchResult searchResult : results) {
            if (searchResult.getName().equalsIgnoreCase(name))
                resultsList.add(searchResult);
        }

        return resultsList.size() == 0 ? results : resultsList;
    }

    public static List<SearchResult> getResultsByPhoneAndState(String state, String phone) {
        List<SearchResult> results = stateResults.get(state);
        List<SearchResult> resultsList = new LinkedList<>();
        for (SearchResult searchResult : results) {
            if (searchResult.getPhone().trim().equalsIgnoreCase(phone.trim()))
                resultsList.add(searchResult);
        }

        return resultsList.size() == 0 ? results : resultsList;
    }

    public static List<SearchResult> getResultsByEmail(String email) {
        List<SearchResult> results = emailResults.get(email);
        if (results == null) {
            String state = getRandomState();
            SearchKeeper.setState(state);
            return getResultsByState(state);
        }
        return results;
    }

    public static String getRandomState() {
        List<String> states = getStates();
        int randomState = new Random().nextInt(states.size() - 1);
        return states.get(randomState);
    }

    public static List<SearchResult> getResultsByPhone(String phone) {
        return phoneResults.get(phone);
    }
}
