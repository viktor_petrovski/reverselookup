package org.people.search.people_search.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;

import org.people.search.people_search.R;
import org.people.search.people_search.custom.AdvancedWebView;

public class WebContainerFragment extends AdFragment implements View.OnClickListener {

    private AdvancedWebView browser;
    public WebContainerFragment() {}

    @Override
    int getCurrentFrame() {
        return 8;
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState ) {
        View rootView = inflater.inflate( R.layout.fragment_webview, container, false );
        initAdBanner( rootView );
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        initWidgets( getView() );
    }

    private void initWidgets( View rootView ) {
        getActivity().findViewById( R.id.web_view_back ).setOnClickListener( this );
        getActivity().findViewById( R.id.web_view_next ).setOnClickListener( this );
        initBrowser( rootView );
    }

    private void initBrowser( View rootView ) {
        browser = ( AdvancedWebView ) rootView.findViewById( R.id.webView );

        browser.requestFocus();
        browser.getSettings().setJavaScriptEnabled( true );
        browser.getSettings().setBuiltInZoomControls( false );
        browser.getSettings().setAppCacheEnabled( true );
        browser.getSettings().setDatabaseEnabled( true );
        browser.getSettings().setDomStorageEnabled( true );
        browser.setGeolocationEnabled( true );
        browser.getSettings().setPluginState( WebSettings.PluginState.ON );
        browser.loadUrl( getString( R.string.web_view_link ) );
    }

    @Override
    public void onClick( View v ) {
        switch ( v.getId() ) {
            case R.id.web_view_next:
                if ( browser.canGoForward() ) {
                    browser.goForward();
                }
                break;
            case R.id.web_view_back:
                if ( browser.canGoBack() ) {
                    browser.goBack();
                }
                break;
        }
    }
}
