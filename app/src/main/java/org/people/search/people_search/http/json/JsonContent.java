package org.people.search.people_search.http.json;

import java.util.List;

public class JsonContent {
    private List< SuggestedApplication > apps;
    private boolean rating;
    private boolean container;
    private String version;

    public JsonContent() {}

    public List< SuggestedApplication > getApps() { return apps; }
    public void setApps( List< SuggestedApplication > apps ) { this.apps = apps; }

    public boolean isRating() { return rating; }
    public void setRating( boolean rating ) { this.rating = rating; }

    public boolean isContainer() { return container; }
    public void setContainer( boolean container ) { this.container = container; }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }
}
