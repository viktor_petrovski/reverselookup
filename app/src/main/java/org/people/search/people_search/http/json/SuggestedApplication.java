package org.people.search.people_search.http.json;

public class SuggestedApplication {
    private String title;
    private String description;
    private String imageLink;
    private String appLink;

    public SuggestedApplication() {}

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageLink() {
        return imageLink;
    }
    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public String getAppLink() {
        return appLink;
    }
    public void setAppLink(String appLink) {
        this.appLink = appLink;
    }
}
