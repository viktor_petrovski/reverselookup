package org.people.search.people_search;

import android.content.Context;
import android.content.SharedPreferences;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.people.search.people_search.http.json.JsonContent;
import org.people.search.people_search.people_search.Const;

import java.io.InputStream;

/**
 * Created by Victor on 8/6/17.
 */

public class Util {

    public static String getSearchVersion(Context context) {

        SharedPreferences preferences = context.getSharedPreferences(Const.PREFERENCES, Context.MODE_PRIVATE);
        String version = preferences.getString(Const.PREFERENCES_VERSION, "no_version");

        if (!version.equals("no_version"))
            return version;

        InputStream inputStream = null;

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            inputStream = context.getAssets().open("settings.json");

            JsonContent content = objectMapper.readValue(inputStream, JsonContent.class);

            return content.getVersion();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return "";
    }
}
