package org.people.search.people_search.fragments.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import org.people.search.people_search.people_search.Const;
import org.people.search.people_search.fragments.DetailedReportFragment;
import org.people.search.people_search.fragments.EnterNameFragment;
import org.people.search.people_search.fragments.MapDetailsFragment;
import org.people.search.people_search.fragments.RatingFragment;
import org.people.search.people_search.fragments.SearchResultFragment;
import org.people.search.people_search.fragments.SplashFragment;
import org.people.search.people_search.fragments.SuggestedApplicationsFragment;
import org.people.search.people_search.fragments.ThanksFragment;
import org.people.search.people_search.fragments.WebContainerFragment;

public class SectionPagerAdapter extends FragmentPagerAdapter {

    private Context context;

    public SectionPagerAdapter( FragmentManager fm, Context context ) {
        super( fm );
        this.context = context;
    }

    @Override
    public Fragment getItem( int position ) {
        Fragment result = null;

        switch ( position ) {
            case Const.SUGGESTED_APPLICATIONS_FRAGMENT:
                result = new SuggestedApplicationsFragment();
            break;
            case Const.ENTER_NAME_FRAGMENT:
                result = new EnterNameFragment();
            break;
            case Const.SPLASH_FRAGMENT:
                result = new SplashFragment();
            break;
            case Const.SEARCH_RESULT_FRAGMENT:
                result = new SearchResultFragment();
            break;
            case Const.MAP_DETAILS_FRAGMENT:
                result = new MapDetailsFragment();
            break;
            case Const.RATING_FRAGMENT:
                result = new RatingFragment();
            break;
            case Const.DETAILED_REPORT_FRAGMENT:
                result = new DetailedReportFragment();
            break;
            case Const.THANKS_FRAGMENT:
                result = new ThanksFragment();
            break;
            case Const.WEB_VIEW_CONTAINER_FRAGMENT:
                result = new WebContainerFragment();
            break;
        }

        return result;
    }

    @Override
    public int getCount() {
        return 9;
    }
}
