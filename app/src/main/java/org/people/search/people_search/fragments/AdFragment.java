package org.people.search.people_search.fragments;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import org.people.search.people_search.R;
import org.people.search.people_search.people_search.Const;

import java.util.concurrent.atomic.AtomicBoolean;

public abstract class AdFragment extends Fragment {
    private InterstitialAd interstitialAd;
    private AtomicBoolean started = new AtomicBoolean( false );
    private AtomicBoolean loaded = new AtomicBoolean( false );

    private AdView adView;

    public AdFragment() {}

    protected void initInterstitialAd() {
        interstitialAd = new InterstitialAd( getActivity() );
        interstitialAd.setAdUnitId( getString( R.string.ad_interstitial ) );

        AdRequest adRequest = new AdRequest.Builder().
                //addTestDevice( "DA95C8F2D63425AF011D47E2D97586FD" ).
                build();

        interstitialAd.loadAd( adRequest );
        interstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                loaded.set( true );
                if ( started.get() ) {
                    interstitialAd.show();
                }
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
                started.set( false );
                loaded.set( false );
                interstitialAd.loadAd( new AdRequest.Builder().build() );

                if ( !customOnAdClosedHandler() ) {
                    LocalBroadcastManager.getInstance( getContext() ).sendBroadcast( new Intent( Const.NEXT_PAGE ) );
                }
            }
        } );
    }

    protected void showInterstitialAd() {
        if ( loaded.get() ) {
            interstitialAd.show();
        } else {
            started.set( true );
            initInterstitialAd();

        }
    }

    abstract int getCurrentFrame();

    protected boolean customOnAdClosedHandler() { return false; }

    protected void initAdBanner( View rootView ) {
        adView = ( AdView ) rootView.findViewById( R.id.adView );
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd( adRequest );
    }

    @Override
    public void onPause() {
        if ( adView != null ) {
            adView.pause();
        }

        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if ( adView != null ) {
            adView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if ( adView != null ) {
            adView.destroy();
        }

        super.onDestroy();
    }
}
