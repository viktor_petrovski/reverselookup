package org.people.search.people_search;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import org.people.search.people_search.fragments.adapter.SectionPagerAdapter;
import org.people.search.people_search.people_search.AddressesKeeper;
import org.people.search.people_search.people_search.Const;

import java.util.concurrent.atomic.AtomicBoolean;

public class MainActivity extends AppCompatActivity {

    private SectionPagerAdapter sectionPagerAdapter;
    private ViewPager viewPager;

    private InterstitialAd interstitialAd;
    private AtomicBoolean started = new AtomicBoolean( false );
    private AtomicBoolean loaded = new AtomicBoolean( false );

    public static int state = 0;

    private LocalBroadcastManager localBroadcastManager;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive( Context context, Intent intent ) {
            switch ( intent.getAction() ) {
                case Const.NEXT_PAGE:
                    findViewById( R.id.toolbar ).setVisibility( View.VISIBLE );
                    if ( viewPager.getAdapter().getCount() > viewPager.getCurrentItem() + 1 ) {
                        if ( viewPager.getCurrentItem() == Const.SUGGESTED_APPLICATIONS_FRAGMENT ) {
                            SharedPreferences preferences = context.getSharedPreferences( Const.PREFERENCES, Context.MODE_PRIVATE );
                            if ( preferences.getBoolean( Const.PREFERENCES_RATING, false ) ) {
                                viewPager.setCurrentItem( viewPager.getCurrentItem() + 1, false );
                            } else {
                                viewPager.setCurrentItem( viewPager.getCurrentItem() + 2, false );
                            }
                        } else {
                            viewPager.setCurrentItem( viewPager.getCurrentItem() + 1, false );
                        }

                        state = viewPager.getCurrentItem();
                        if ( state == Const.SEARCH_RESULT_FRAGMENT ) {
                            localBroadcastManager.sendBroadcast( new Intent( Const.UPDATE_SEARCH_RESULTS ) );
                        } else if ( state == Const.MAP_DETAILS_FRAGMENT ) {
                            localBroadcastManager.sendBroadcast( new Intent( Const.UPDATE_MAP_POSITION ) );
                        }
                    }
                break;
                case Const.SHOW_WEB_VIEW:
                    findViewById( R.id.web_view_back ).setVisibility( View.VISIBLE );
                    findViewById( R.id.web_view_next ).setVisibility( View.VISIBLE );
                    state = Const.WEB_VIEW_CONTAINER_FRAGMENT;

                    viewPager.setCurrentItem( Const.WEB_VIEW_CONTAINER_FRAGMENT, false );
                break;
                case Const.EXIT:
                    showExitDialog();
                break;
            }
        }
    };

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        initToolbar();
        initAddressesKeeper();
        initWidgets();
        initBroadcastManager();
        initInterstitialAd();
    }

    private void initToolbar() {
        findViewById( R.id.toolbar ).setVisibility( View.GONE );
    }

    private void initAddressesKeeper() {
        AddressesKeeper.init( getApplicationContext() );
    }

    private void initBroadcastManager() {
        IntentFilter intentFilter = new IntentFilter( Const.NEXT_PAGE );
        intentFilter.addAction( Const.SHOW_WEB_VIEW );
        intentFilter.addAction( Const.EXIT );

        localBroadcastManager = LocalBroadcastManager.getInstance( this );
        localBroadcastManager.registerReceiver( receiver, intentFilter );
    }

    private void initWidgets() {
        Toolbar toolbar = ( Toolbar ) findViewById( R.id.toolbar );
        setSupportActionBar( toolbar );

        sectionPagerAdapter = new SectionPagerAdapter( getSupportFragmentManager(), this );
        viewPager = ( ViewPager ) findViewById( R.id.container );
        viewPager.setAdapter( sectionPagerAdapter );
        viewPager.setOffscreenPageLimit( 7 );
    }

    @Override
    public void onBackPressed() {
        if ( state == Const.WEB_VIEW_CONTAINER_FRAGMENT || state == Const.THANKS_FRAGMENT ) {
            if ( loaded.get() ) {
                interstitialAd.show();
            } else {
                started.set( true );
            }
        } else if ( viewPager.getCurrentItem() >= 2 ) {
            if ( viewPager.getCurrentItem() == Const.ENTER_NAME_FRAGMENT ) {
                SharedPreferences preferences = getSharedPreferences( Const.PREFERENCES, Context.MODE_PRIVATE );
                if ( preferences.getBoolean( Const.PREFERENCES_RATING, false ) ) {
                    viewPager.setCurrentItem( viewPager.getCurrentItem() - 1, false );
                } else {
                    viewPager.setCurrentItem( viewPager.getCurrentItem() - 2, false );
                }
            } else {
                viewPager.setCurrentItem( viewPager.getCurrentItem() - 1, false );
            }

            state = viewPager.getCurrentItem();
        }
    }

    private void initInterstitialAd() {
        interstitialAd = new InterstitialAd( this );
        interstitialAd.setAdUnitId( getString( R.string.ad_interstitial ) );

        AdRequest adRequest = new AdRequest.Builder().build();

        interstitialAd.loadAd( adRequest );
        interstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                loaded.set( true );
                if ( started.get() ) {
                    interstitialAd.show();
                }
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
                loaded.set( false );
                started.set( false );

                interstitialAd.loadAd( new AdRequest.Builder().build() );
                showExitDialog();
            }
        } );
    }

    private void showExitDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder( this );
        builder.setMessage( R.string.dialog_close_message )
                .setPositiveButton( R.string.dialog_close_message_yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick( DialogInterface dialog, int id ) {
                                dialog.dismiss();
                                finish();
                            }
                        } )
                .setNegativeButton( R.string.dialog_close_message_cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick( DialogInterface dialog, int id ) {
                                loaded.set( false );
                                started.set( false );
                                initInterstitialAd();
                                dialog.dismiss();
                            }
                        } );

        builder.create().show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        localBroadcastManager.unregisterReceiver( receiver );
    }
}
