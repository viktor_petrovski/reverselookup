package org.people.search.people_search.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import org.people.search.people_search.R;
import org.people.search.people_search.people_search.Const;
import org.people.search.people_search.http.SendEmailTask;

public class DetailedReportFragment extends AdFragment implements View.OnClickListener {
    public DetailedReportFragment() {}

    @Override
    int getCurrentFrame() {
        return Const.DETAILED_REPORT_FRAGMENT;
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState ) {
        View rootView = inflater.inflate( R.layout.fragment_detailed_report, container, false );
        initAdBanner( rootView );
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        initWidgets( getView() );
    }

    private void initWidgets( View rootView ) {
        EditText editText = ( EditText ) rootView.findViewById( R.id.email );
        editText.setOnEditorActionListener( new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction( TextView v, int actionId, KeyEvent event ) {
                if ( actionId == EditorInfo.IME_ACTION_DONE ) {
                    sendEmail();
                }

                return false;
            }
        } );

        rootView.findViewById( R.id.send_email ).setOnClickListener( this );
    }

    @Override
    public void onClick( View v ) {
        switch ( v.getId() ) {
            case R.id.send_email:
                sendEmail();
            break;
        }
    }

    private void sendEmail() {
        getView().findViewById( R.id.loading ).setVisibility( View.VISIBLE );
        getView().findViewById( R.id.send_email ).setVisibility( View.GONE );

        SendEmailTask task = new SendEmailTask() {
            @Override
            protected void onPostExecute( Integer code ) {
                super.onPostExecute( code );

                if ( Const.SEND_SUCCESSFULLY == code ) {
                    LocalBroadcastManager.getInstance( getContext() ).sendBroadcast( new Intent( Const.NEXT_PAGE ) );
                } else {
                    new AlertDialog.Builder( getActivity() ).
                            setMessage( getString( R.string.wrong_email ) ).
                            setPositiveButton( getString( R.string.close ), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick( DialogInterface dialog, int which ) {
                                    dialog.dismiss();

                                    getView().findViewById( R.id.loading ).setVisibility( View.GONE );
                                    getView().findViewById( R.id.send_email ).setVisibility( View.VISIBLE );
                                }
                            } ).show();
                }
            }
        };
        task.execute( getString( R.string.campaign_monitor_url ),
                      ( ( EditText ) getView().findViewById( R.id.email ) ).getText().toString(),
                      getString( R.string.campaign_monitor_list_id ),
                      getString( R.string.campaign_monitor_api_key ) );
    }
}
