package org.people.search.people_search.http;

import com.squareup.okhttp.ResponseBody;

import org.people.search.people_search.http.json.Subscriber;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.POST;
import retrofit.http.Path;

public interface ServerService {
    @POST( "/api/v3.1/subscribers/{listId}.json" )
    Call< ResponseBody > addSubscriber( @Body Subscriber subscriber,
                                        @Path( "listId" ) String listId );
}

