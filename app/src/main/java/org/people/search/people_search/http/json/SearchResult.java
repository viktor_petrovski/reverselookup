package org.people.search.people_search.http.json;

public class SearchResult {

    private int id;
    private String name;
    private String phone;
    private String email;
    private String address;

    public SearchResult() {}

    public SearchResult( int lineNumber, String address, String phone, String name, String email ) {
        this.id = lineNumber;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.name = name;
    }

    public int getId() { return id; }
    public void setId( int id ) { this.id = id; }

    public String getName() { return name; }
    public void setName( String name ) { this.name = name; }

    public String getPhone() { return phone; }
    public void setPhone( String phone ) { this.phone = phone; }

    public String getEmail() { return email; }
    public void setEmail( String email ) { this.email = email; }

    public String getAddress() { return address; }
    public void setAddress( String address ) { this.address = address; }
}
