package org.people.search.people_search.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.people.search.people_search.R;
import org.people.search.people_search.people_search.Const;

public class RatingFragment extends AdFragment implements View.OnClickListener {

    private static Boolean fromPlayMarket = false;

    public RatingFragment() {}

    @Override
    int getCurrentFrame() {
        return Const.RATING_FRAGMENT;
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState ) {
        View rootView = inflater.inflate( R.layout.fragment_rating, container, false );
        initAdBanner( rootView );
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        if ( fromPlayMarket ) {
            fromPlayMarket = false;
            LocalBroadcastManager.getInstance( getContext() ).sendBroadcast( new Intent( Const.NEXT_PAGE ) );
        } else {
            initWidgets( getView() );
        }
    }

    private void initWidgets( View rootView ) {
        SharedPreferences preferences = getContext().getSharedPreferences( Const.PREFERENCES, Context.MODE_PRIVATE );

        boolean showRating = preferences.getBoolean( Const.PREFERENCES_RATING, false );
        if ( showRating ) {
            rootView.findViewById( R.id.rating_wrapper ).setVisibility( View.VISIBLE );
            rootView.findViewById( R.id.non_rating_wrapper ).setVisibility( View.GONE );
        } else {
            rootView.findViewById( R.id.rating_wrapper ).setVisibility( View.GONE );
            rootView.findViewById( R.id.non_rating_wrapper ).setVisibility( View.VISIBLE );
        }

        rootView.findViewById( R.id.rating_five_stars ).setOnClickListener( this );
        rootView.findViewById( R.id.rating_application_icon ).setOnClickListener( this );
        rootView.findViewById( R.id.rating_screen_next_button ).setOnClickListener( this );
    }

    @Override
    public void onClick( View v ) {
        switch ( v.getId() ) {
            case R.id.rating_screen_next_button:
                showInterstitialAd();
                break;
            case R.id.rating_application_icon:
            case R.id.rating_five_stars:
                fromPlayMarket = true;
                startActivity( new Intent( Intent.ACTION_VIEW, Uri.parse( getString( R.string.google_play_uri ) ) ) );
                break;
        }
    }
}
