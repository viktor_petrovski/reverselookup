package org.people.search.people_search.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.people.search.people_search.R;
import org.people.search.people_search.fragments.adapter.SuggestedApplicationsAdapter;
import org.people.search.people_search.http.json.JsonContent;
import org.people.search.people_search.http.json.SuggestedApplication;
import org.people.search.people_search.people_search.Const;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

public class SuggestedApplicationsFragment extends AdFragment {

    private ListView listView;
    private boolean container;

    public SuggestedApplicationsFragment() {
    }

    @Override
    int getCurrentFrame() {
        return Const.SUGGESTED_APPLICATIONS_FRAGMENT;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_suggested_applications, container, false);
        initInterstitialAd();

        listView = (ListView) rootView.findViewById(R.id.list_of_apps);
        initAppsContent();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SuggestedApplication app = (SuggestedApplication) view.getTag();

                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(app.getAppLink())));
                } catch (Exception e) {
                    Toast.makeText(getContext(), "Error has been occurred", Toast.LENGTH_SHORT).show();
                }
            }
        });

        setHasOptionsMenu(true);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_skip, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.skip:
                showInterstitialAd();
                return true;
        }
        return false;
    }

    @Override
    protected boolean customOnAdClosedHandler() {
        if (container) {
            LocalBroadcastManager.getInstance(getContext()).sendBroadcast(new Intent(Const.SHOW_WEB_VIEW));
        } else {
            LocalBroadcastManager.getInstance(getContext()).sendBroadcast(new Intent(Const.NEXT_PAGE));
        }

        return true;
    }

    private void initAppsContent() {
        AsyncTask<Void, Integer, JsonContent> task = new AsyncTask<Void, Integer, JsonContent>() {
            @Override
            protected JsonContent doInBackground(Void... params) {
                try {
                    URL url = new URL(getString(R.string.suggested_applications_link));
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setConnectTimeout(60000);

                    BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuilder contentBuilder = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        contentBuilder.append(line);
                    }
                    reader.close();

                    ObjectMapper objectMapper = new ObjectMapper();
                    return objectMapper.readValue(contentBuilder.toString(), JsonContent.class);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(JsonContent jsonContent) {
                super.onPostExecute(jsonContent);

                saveContent(jsonContent);
                if (jsonContent != null) {
                    listView.setAdapter(new SuggestedApplicationsAdapter(getContext(), jsonContent.getApps()));
                } else {
                    listView.setAdapter(new SuggestedApplicationsAdapter(getContext(), getApplicationsFromAsset()));
                }
            }
        };
        task.execute();
    }

    private List<SuggestedApplication> getApplicationsFromAsset() {
        InputStream inputStream = null;

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            inputStream = getContext().getAssets().open("settings.json");

            JsonContent content = objectMapper.readValue(inputStream, JsonContent.class);
            saveContent(content);

            return content.getApps();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return new LinkedList();
    }

    private void saveContent(JsonContent content) {
        if (content != null) {
            container = content.isContainer();

            SharedPreferences preferences = getContext().getSharedPreferences(Const.PREFERENCES, Context.MODE_PRIVATE);
            preferences.edit().putBoolean(Const.PREFERENCES_RATING, content.isRating()).apply();
            preferences.edit().putString(Const.PREFERENCES_VERSION, content.getVersion()).apply();
        }
    }
}
