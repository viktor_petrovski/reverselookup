package org.people.search.people_search.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.people.search.people_search.R;
import org.people.search.people_search.people_search.Const;

public class ThanksFragment extends AdFragment implements View.OnClickListener {
    public ThanksFragment() {}

    @Override
    int getCurrentFrame() {
        return Const.THANKS_FRAGMENT;
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState ) {
        View rootView = inflater.inflate( R.layout.fragment_thanks, container, false );
        initAdBanner( rootView );
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        initWidgets( getView() );
    }

    private void initWidgets( View rootView ) {
        rootView.findViewById( R.id.exit ).setOnClickListener( this );
    }

    @Override
    public void onClick( View v ) {
        switch ( v.getId() ) {
            case R.id.exit:
                LocalBroadcastManager.getInstance( getContext() ).sendBroadcast( new Intent( Const.EXIT ) );
            break;
        }
    }
}
