package org.people.search.people_search.fragments.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.people.search.people_search.R;
import org.people.search.people_search.http.json.SuggestedApplication;

import java.util.List;

public class SuggestedApplicationsAdapter extends ArrayAdapter< SuggestedApplication > {

    private LayoutInflater layoutInflater;

    public SuggestedApplicationsAdapter( Context context, List< SuggestedApplication > objects ) {
        super( context, -1, objects );
        layoutInflater = LayoutInflater.from( context );
    }

    @Override
    public View getView( int position, View convertView, ViewGroup parent ) {
        View view = layoutInflater.inflate( R.layout.item_suggested_application, parent, false );

        SuggestedApplication item = getItem( position );

        ImageView thumb = (ImageView ) view.findViewById( R.id.thumbnail );
        Picasso.with( getContext().getApplicationContext() )
                .load( item.getImageLink() )
                .into( thumb );

        TextView text = ( TextView ) view.findViewById(R.id.text);
        text.setText(item.getTitle());

        TextView description = (TextView ) view.findViewById(R.id.description);
        description.setText(item.getDescription());

        view.setTag(item);
        return view;
    }
}