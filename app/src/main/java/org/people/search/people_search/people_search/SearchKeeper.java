package org.people.search.people_search.people_search;

import org.people.search.people_search.http.json.SearchResult;

public class SearchKeeper {

    /*
    * 1 Searching by name
    * 2 Searchibg by email
    * 3 Searching by phone;
    * */
    public static final int SEARCH_BY_NAME = 1;
    public static final int SEARCH_BY_EMAIL = 2;
    public static final int SEARCH_BY_PHONE = 3;

    private static int search_cirteria;

    private static String name;
    private static String state;

    private static String phone;
    private static String email;

    private static SearchResult searchResult;
    private static float latitude;
    private static float longitude;
    private static int position;
    private static int size;

    public static String getPhone() {
        return phone;
    }

    public static void setPhone(String phone) {
        SearchKeeper.phone = phone;
    }

    public static String getEmail() {
        return email;
    }

    public static void setEmail(String email) {
        SearchKeeper.email = email;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        SearchKeeper.name = name.toLowerCase();
    }

    public static String getState() {
        return state;
    }

    public static void setState(String state) {
        SearchKeeper.state = state;
    }

    public static SearchResult getSearchResult() {
        return searchResult;
    }

    public static void setSearchResult(SearchResult searchResult) {
        SearchKeeper.searchResult = searchResult;
    }

    public static float getLatitude() {
        return latitude;
    }

    public static void setLatitude(float latitude) {
        SearchKeeper.latitude = latitude;
    }

    public static float getLongitude() {
        return longitude;
    }

    public static void setLongitude(float longitude) {
        SearchKeeper.longitude = longitude;
    }

    public static void setPosition(int position) {
        SearchKeeper.position = position;
    }

    public static int getPosition() {
        return position;
    }

    public static int getSize() {
        return size;
    }

    public static void setSize(int size) {
        SearchKeeper.size = size;
    }

    public static void setSearch_cirteria(int search_cirteria) {
        SearchKeeper.search_cirteria = search_cirteria;
    }

    public static int getSearch_cirteria() {
        return search_cirteria;
    }

    public static void resetData(){
        setPhone(null);
        setEmail(null);
        setState(null);
        setPosition(0);
        setSize(0);
    }
}
