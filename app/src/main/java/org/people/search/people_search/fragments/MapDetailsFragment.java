package org.people.search.people_search.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.people.search.people_search.R;
import org.people.search.people_search.people_search.Const;
import org.people.search.people_search.people_search.SearchKeeper;
import org.people.search.people_search.http.json.SearchResult;

import java.util.List;
import java.util.Random;

public class MapDetailsFragment extends AdFragment implements View.OnClickListener {

    private static Random random = new Random( System.currentTimeMillis() );
    private MapView mapView;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive( final Context context, Intent intent ) {
            if ( SearchKeeper.getSearchResult() == null ) {
                return;
            }

            updateInfo();
            updateAddress();

            mapView.getMapAsync( new OnMapReadyCallback() {
                @Override
                public void onMapReady( GoogleMap map ) {
                    updateMap( map );
                }
            } );
        }
    };

    private void updateInfo() {
        SearchResult result = SearchKeeper.getSearchResult();

        TextView text = ( TextView ) getView().findViewById( R.id.phone_value );
        text.setText( result.getPhone() );

        text = ( TextView ) getView().findViewById( R.id.email_value );
        text.setText( result.getEmail() );
    }

    private void updateMap( GoogleMap map ) {
        GoogleMap googleMap = map;
        googleMap.clear();

        LatLng position = new LatLng( SearchKeeper.getLatitude(), SearchKeeper.getLongitude() );
        googleMap.addMarker( new MarkerOptions().position( position ).title( SearchKeeper.getName() ) ).showInfoWindow();

        CameraPosition cameraPosition = new CameraPosition.Builder().target( position ).zoom( 12 ).build();
        googleMap.moveCamera( CameraUpdateFactory.newCameraPosition( cameraPosition ) );
    }

    private void updateAddress() {
        Geocoder geocoder = new Geocoder( getContext() );
        try {
            List< Address > addresses = geocoder.getFromLocationName(
                    getAddressForRequest( SearchKeeper.getSearchResult().getAddress() ), 1 );
            if ( addresses.size() > 0 ) {
                SearchKeeper.setLatitude( ( float ) addresses.get( 0 ).getLatitude() +
                        ( random.nextFloat() - 0.5f ) / 100 );
                SearchKeeper.setLongitude( ( float ) addresses.get( 0 ).getLongitude() +
                        ( random.nextFloat() - 0.5f ) / 100 );
            }
        } catch ( Exception e ) {}
    }

    private String getAddressForRequest( String address ) {
        String[] parts = address.split( "," );
        return parts[ 1 ] + ", " + parts[ 2 ];
    }

    public MapDetailsFragment() {}

    @Override
    int getCurrentFrame() {
        return Const.MAP_DETAILS_FRAGMENT;
    }

    @Override
    public void onCreate( @Nullable Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        LocalBroadcastManager.getInstance( getContext() ).registerReceiver( receiver,
                new IntentFilter( Const.UPDATE_MAP_POSITION ) );
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance( getContext() ).unregisterReceiver( receiver );
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState ) {
        View rootView = inflater.inflate( R.layout.fragment_map_details, container, false );
        initAdBanner( rootView );
        initGoogleMaps( rootView, savedInstanceState );
        return rootView;
    }

    private void initGoogleMaps( View rootView, Bundle savedInstanceState ) {
        mapView = ( MapView ) rootView.findViewById( R.id.mapView );
        mapView.onCreate( savedInstanceState );

        mapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize( getActivity().getApplicationContext() );
        } catch ( Exception e ) {
            e.printStackTrace();
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        initWidgets( getView() );
    }

    private void initWidgets( View rootView ) {
        rootView.findViewById( R.id.get_detailed_report ).setOnClickListener( this );
        rootView.findViewById( R.id.map_info ).setOnClickListener( this );
    }

    @Override
    public void onClick( View v ) {
        switch ( v.getId() ) {
            case R.id.get_detailed_report:
                LocalBroadcastManager.getInstance( getContext() ).sendBroadcast( new Intent( Const.NEXT_PAGE ) );
            break;
            case R.id.map_info:
                startActivity( new Intent( Intent.ACTION_VIEW, Uri.parse( getString( R.string.map_details_click_url ) ) ) );
            break;
        }
    }

}
