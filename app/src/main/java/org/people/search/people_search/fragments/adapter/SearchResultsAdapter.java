package org.people.search.people_search.fragments.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.people.search.people_search.R;
import org.people.search.people_search.people_search.Const;
import org.people.search.people_search.people_search.SearchKeeper;
import org.people.search.people_search.http.json.SearchResult;

import java.util.List;

public class SearchResultsAdapter extends ArrayAdapter<SearchResult> {

    private LayoutInflater layoutInflater;

    public SearchResultsAdapter(Context context, List<SearchResult> objects) {
        super(context, -1, objects);
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = layoutInflater.inflate(R.layout.item_search_result, parent, false);

        SearchResult item = getItem(position);

        String name = SearchKeeper.getSearch_cirteria() == SearchKeeper.SEARCH_BY_NAME ? SearchKeeper.getName() : item.getName();
        String phone = SearchKeeper.getSearch_cirteria() == SearchKeeper.SEARCH_BY_PHONE ? SearchKeeper.getPhone() : item.getPhone();
        String mail = SearchKeeper.getSearch_cirteria() == SearchKeeper.SEARCH_BY_EMAIL ? SearchKeeper.getEmail() : item.getEmail();

        TextView text = (TextView) view.findViewById(R.id.name_value);
        text.setText(name);

        text = (TextView) view.findViewById(R.id.phone_value);
        text.setText(phone);

        text = (TextView) view.findViewById(R.id.email_value);
        text.setText(mail);

        text = (TextView) view.findViewById(R.id.address_value);
        text.setText(item.getAddress());

        view.findViewById(R.id.detailed_report).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchResult searchResult = getItem(position);
                SearchKeeper.setSearchResult(searchResult);
                LocalBroadcastManager.getInstance(getContext()).sendBroadcast(new Intent(Const.NEXT_PAGE));
            }
        });

        return view;
    }
}