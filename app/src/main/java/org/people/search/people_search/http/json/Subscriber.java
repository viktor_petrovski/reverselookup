package org.people.search.people_search.http.json;

public class Subscriber {
    private String EmailAddress;
    private boolean Resubscribe = true;
    private boolean RestartSubscriptionBasedAutoresponders = true;

    public Subscriber( String emailAddress ) {
        this.EmailAddress = emailAddress;
    }

    public String getEmailAddress() { return EmailAddress; }
    public boolean isRestartSubscriptionBasedAutoresponders() { return RestartSubscriptionBasedAutoresponders; }
    public boolean isResubscribe() { return Resubscribe; }
}
